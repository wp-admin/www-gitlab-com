---
layout: job_family_page
title: "Sr Sales Operations Manager"
---

## Responsibilities

* Support the field sales team on quote processes, pricing, configuration, and terms
* Collaborate with the finance deal desk team to ensure that quote creation and approval SLAs are met
* Review non-standard deal terms and ensure compliance with published sales and approval policies and act as escalation point for approvals
* Provide first-tier support for any end-user technical or process questions in Salesforce and other systems primarily managed by Sales Operations
* Implement tools and processes for the sales organization that focus on improving efficiency, effectiveness, and productivity
* Identify opportunities for process automation and optimization, with a focus on scalability and driving significant growth
* Work with other departments to improve integration between Salesforce and other mission-critical systems, including Marketo, Zuora, and Zendesk
* Collaborate with Marketing to ensure proper lead management processes, metrics and policies
* Maintain data integrity within customer records in Salesforce.com and other systems
* Monitor system adoption and data compliance and governance
* Develop best practices that align sales data quality with company initiatives

## Requirements

* BA/BS degree
* Minimum 5 years relevant experience and solid understanding of sales operations
* Strong analytical ability and able to prioritize multiple projects
* Managed Deal Desk functions and an expert with CPQ tools
* Deep SFDC expertise and knowledge of enterprise SaaS tools
* Excellent problem solving, project management, interpersonal and organizational skills
* SaaS and B2B experience preferred
* Interest in GitLab, and open source software
* You share our values, and work in accordance with those values.