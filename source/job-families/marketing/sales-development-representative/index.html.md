---
layout: job_family_page
title: "Sales Development Representative"
---

If you love talking about tech, learning about the latest and greatest, and want to continue to grow your career in software sales, GitLab's Sales Development Rep role could be a role worth exploring for you. In addition to having an affinity for tech and experience in sales, we want to build a team that thrives on asking questions, addressing challenges, and nurturing customers.
A successful SDR will take initiative to meet sales goals, qualify leads, build positive and lasting relationships with prospective clients, and support future and existing clients in their adoption of GitLab. Most importantly, our marketing team, including SDRs will use GitLab itself to document and update sales best practices and the company [handbook](/handbook).
As part of our marketing organization, the SDR will report to the SDR Lead, and will work very closely with both Strategic Account Executives and Regional Sales Directors.

### Requirements

* Twelve months minimum experience in sales or client success in a software/tech company
* An understanding of B2B software sales, open source software, and the developer product space
* Affinity for software and knowledge of the software development process
* Ability to carefully listen
* Desire for a continued career in software sales
* Passionate about technology and strong desire to learn
* You share our [values](/handbook/values), and work in accordance with those values
* Exceptional written and verbal English communication skills

### Responsibilities

* Meet or exceed Sales Development Representative (SDR) sourced Sales Accepted Opportunity (SAO) volume targets.
* Work with designated strategic account leader to identify and prioritize key accounts to develop
* Working with the sales team to identify net-new opportunities and nurture existing opportunities within target accounts.
* Work in collaboration with field and corporate marketing to drive attendance at marketing events
* Attend field marketing events to engage with participants identify opportunities, and to schedule meetings
* Act as a mentor for new SDR hires in helping them navigate their key accounts.
* Work in collaboration with Online Marketing to develop targeted marketing tactics against your assigned target accounts.

## Senior Sales Development Representative

As a Senior Sales Development Representative (SDR) you will develop interest in GitLab among the largest accounts served by our Strategic Account Leaders. In addition, it is expected that you will lead the SDR team from the front in every way that the team is measured. You will be a source of knowledge and best practices amongst the outbound SDRs, and will help to train, onboard, and mentor new SDRs.

### Responsibilities

* Meet or exceed Senior Sales Development Representative (SDR) sourced Sales Accepted Opportunity (SAO) volume targets.
* Act as a mentor for your SDR team and assist new hires in learning the prospecting tools, processes, messaging, target personas and how to navigate their key accounts.
* Act as a catalyst for asynchronous collaboration with achievement recognition, best practice sharing and prefacing new ideas or processes through Slack and other channels.
* Review/work accounts in the strategic segment in alignment with GitLab's go-to-market priorities.
* Work in collaboration with Online Marketing to develop targeted marketing tactics against your assigned target accounts.
* Work in collaboration with Field Marketing to drive attendance at field marketing events.
* Attend field marketing events to engage with participants, identify opportunities and to schedule meetings.
* Work in collaboration with Content and Product Marketing to develop effective messaging for outbound communications.
* Work with Regional Sales Directors to identify and prioritize key accounts to develop.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the SDR Manager for the region they have applied for
* Candidates will then be invited to schedule an interview with our Sales Development Manager of Onboarding and Enablement 
* Candidates will then be invited to schedule an interview with our Director of Sales Development
* Candidates may be invited to interview with the Regional Director of Sales for the region
* Successful candidates will subsequently be made an offer

Additional details about our process can be found on our [hiring page](/handbook/hiring).
