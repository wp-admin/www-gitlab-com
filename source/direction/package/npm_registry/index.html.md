---
layout: markdown_page
title: "Category Vision - NPM Registry"
---

- TOC
{:toc}

## NPM Registry

JavaScript developers need a secure, standardized way to share and version control NPM packages across projects. An NPM registry offers developers of lower-level services a way to publish their code in such a way; built directly into GitLab.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=NPM%20Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

## What's Next & Why

TODO

## Competitive Landscape

TODO

## Top Customer Success/Sales Issue(s)

TODO

## Top Customer Issue(s)

TODO

## Top Internal Customer Issue(s)

TODO

## Top Vision Item(s)

TODO
