---
layout: markdown_page
title: Identifying the Cause of IP Blocks on GitLab.com
category: Troubleshooting Guides
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

At times users of GitLab.com can find that their IP address has been blocked due to rate limiting. Currently, rate limit parameters on GitLab.com are best described in [this issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/41308#note_87693185). When this happens we *may* be able to determine what caused a block and relay that information back to the user.

## Searching

### Search Condition

Start with an IP address provided by the user as the search condition. You can then drill down from there with positive and negative filters on fields to get the best results.

An example search for activity from an IP address would be `json.remote_ip:"127.0.0.1"`.

### Fields

#### Primary

The following fields are the best add to your search query in order to get the most important details on multiple requests at a glance.

- `json.status` - Outputs the HTTP status code that was returned for the request. We're usually looking for `401` (Unauthorized) and/or `403` (Forbidden).
- `json.path` - The path on GitLab.com that was accessed by the request or the API endpoint that was hit.
- `json.method` - Can be either `GET`, `POST`, `PUT`, `PATCH`, or `DELETE`. The first three are the most common.

#### Secondary

These fields can be helpful but aren't essential.

- `json.controller` - Gives you a clue as to what part of GitLab.com was being accessed by a particular request.
- `json.params` - Shows what user made the request, what action was taken, and on what resource it was taken on. This field shows what repository was targeted for requests to the container registry.

## Common Causes

### Container Registry

Numerous failed pushes or pulls to `registry.gitlab.com` can result in an IP block.

You can list all log results for hits on the container registry by searching for the provided IP address and setting a positive filter on `json.path` for `/jwt/auth`.

#### Useful Fields

- `json.status`
- `json.path`
- `json.params`
- `json.controller`

#### Normal Push and Pull Examples

##### Push

Failed pushes to the registry will always have `JwtController` for the `json.controller` field and `/jwt/auth` for the `json.path` field. Watch for `:push,pull` in the `json.params` field, indicating that the request is for a push.

A failed push will look like the following in Kibana.

![Failed Push](/images/support/registry-failed-push.png)

##### Pull

Similar to a push, failed pulls from the registry will always have `JwtController` for the `json.controller` field and `/jwt/auth` for the `json.path` field. However, in `json.params` only `:pull` will be present.

A failed pull will look like the following in Kibana.

![Failed Pull](/images/support/registry-failed-pull.png)

#### `gitlab-ci-token` Pulls

Users can also become blocked due to registry pulls from the user `gitlab-ci-token` making (normal) unauthed requests to `jwt/auth`.  This user may be exempted from rate limiting in the future, it's being discussed in [this issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/49392).

An example request looks like:

![gitlab-ci-token registry pull request](/images/support/gitlab-ci-token-pull.png)

To filter for these types of requests specifically, add the `json.params` field.

### LFS

Pushes or pulls to repositories containing LFS objects can result in an IP block if the user is unauthorized.

#### Useful Fields

- `json.action`
- `json.controller`
- `json.method`
- `json.status`

#### Examples

##### Push

Failed LFS pushes will always have `upload_authorize` in the `json.action` field, `Projects::LfsStorageController` for the `json.controller` field, and `PUT` for `json.method`.

A failed LFS push will look like the following in Kibana.

![Failed LFS Push](/images/support/lfs-failed-push.png)
