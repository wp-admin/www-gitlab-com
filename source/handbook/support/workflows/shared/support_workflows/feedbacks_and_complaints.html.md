---
layout: markdown_page
title: Feedback and Complaints
category: Support Workflows
---

### On this page
{:.no_toc}

- TOC
{:toc}

---

### Overview

Use this workflow when a customer submits feedback and/or complaints.

### Workflow

In all cases, tag the ticket with the `feedback` tag. We may build a separate feedback channel, but until then, simply mark the Zendesk tickets for review. Our community team and support managers will work through these.

### Bugs

For feedback and complaints involving bugs, please follow the appropriate [escalation procedure](https://about.gitlab.com/handbook/support/workflows/services/support_workflows/issue_escalations.html) and provide the customer with the created links.

Advise the customer that it is best for them to subscribe to the created issue for updates and participate in it directly moving forward rather than through Zendesk. While all issues are important, our product and infrastructure teams will [prioritize the issues](/handbook/support/workflows/services/support_workflows/issue_escalations.html#issue-prioritization) as needed, and support will not have much control of that.

### Feature Proposals

For feature proposals, guide the customer on how to create the issues in either the [Community Edition](https://gitlab.com/gitlab-org/gitlab-ce/issues) or [Enterprise Edition](https://gitlab.com/gitlab-org/gitlab-ee/issues) issue trackers. If it is not a bug, it is preferable that the customer create the issue themselves - this is especially true in cases where the app/feature is working as intended.

### Refunds

If a customer requests a refund, please follow the [Handling Refund Requests](/handbook/support/workflows/shared/support_workflows/handling_refund_requests.html) workflow.

### Venting

There may be cases where a customer will simply be unsatisfied with all available solutions and/or steps taken to solve problems they are facing or have faced. Deescalate the situation the best you can - apologize as necessary, offer solutions or alternatives that may work. Most importantly, in this situation, allow the customer to vent and assure that their feedback is taken very seriously. In some cases, you may want to CC a manager, especially [if you feel threatened or harassed](/handbook/support/#what-if-i-feel-threatened-or-harassed-while-handling-a-support-request). 

It is important to note that apologies should be sincere - this includes not apologizing when not necessary. Phrases like "I'm sorry that you're having trouble..." can sound ingenuine and "script like". Apologies can also be seen as an admission of guilt, which should not happen when a problem is not the fault of GitLab (such as when a feature works as intended but not the way customer wants). 

### Product Feedback

There may be cases where a customer will open a ticket to convey feedback on our products. When these tickets come in, post the feedback in the `#product` (or other appropriate) slack channel as well as a link to the ticket. Let the customer know that the feedback has been passed on to the product team. It may also be useful to ask if they have any other questions or feedback that they would like to convey. Prior to closing the ticket, tag the ticket with the `feedback` tag.