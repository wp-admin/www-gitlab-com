---
layout: markdown_page
title: "BU.1.01 - Backup Configuration Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# BU.1.01 - Backup Configuration

## Control Statement

GitLab configures redundant systems and performs data backups routinely as specificied by management to resume system operations in the event of a system failure.

## Context

We need to demonstrate that GitLab can be restored to a prior point in time in the event the data is corrupted, service interruption or other event that disrupts normal service.  This helps ensure we have redundancy/backups enabled.

## Scope

TBD

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BU.1.01_backup_configuration.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BU.1.01_backup_configuration.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BU.1.01_backup_configuration.md).

## Framework Mapping

* ISO
  * A.18.1.3
* SOC
  * A1.2
* PCI
  * 12.10.1
